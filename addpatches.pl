#! /usr/bin/perl

use strict;
use POSIX;

# prototypes
sub gendiff($$\@);

my $pat = 'leafnode-2.0.0.alpha';
my $bookmark = 'SCRIPT MARKER #1 ';
my $tar = '/bin/tar';
my $diff = '/usr/bin/diff';
my $gzip = 'gzip';
$ENV{'LC_ALL'} = 'C';
$ENV{'PATH'} = '/bin:/usr/bin';

chdir "$ENV{HOME}/public_html/leafnode/beta" or die "chdir: $!";
my @tarballs = sort <$pat*.tar.bz2>;

open F, "HEADER.html" or die "open: $!";
my $lastver;
my $havemarker;
while(<F>) {
    if (/$bookmark/) {
	$havemarker++;
    }
    next unless $havemarker;
    if (/upgrade-[^-]+-to-([^-]+).patch.gz/) {
	$lastver = $1;
	last;
    }
}
close F;

print "last version: $lastver\n";
my $lastver_save = $lastver;
@tarballs = grep { $_ gt "$lastver" }
		map { s/\Q$pat\E(.*)\.tar\.bz2/$1/; $_; } @tarballs;

print "newer versions: ", join(" ", @tarballs), "\n";
my $mostcurrent = $tarballs[$#tarballs];
if (!@tarballs) { $mostcurrent = $lastver; }

my @addtxt;
foreach my $curver (@tarballs) {
    die "cannot generate diff $lastver -> $curver" unless defined gendiff($lastver, $curver, @addtxt);
    $lastver = $curver;
}

print "inserting:\n", join("", @addtxt);

open F, "HEADER.html" or die "open: $!";
my $state = 0;
open O, ">HEADER.html.new" or die "open: $!";
while (<F>) {
    if ($state == 0) {
	s/\Q$lastver_save\E/$mostcurrent/g;
    }
    print O $_;
    if (/$bookmark/) {
	print O join("", @addtxt);
	$state++;
    }
}
close O or die "cannot close: $!";
rename "HEADER.html.new", "HEADER.html" or die "rename: $!";
close F;

exit;

sub gendiff($$\@) {
    my ($last, $cur, $ary) = @_;

    system ($tar, "-xjf", "$pat$last.tar.bz2") and return undef;
    system ($tar, "-xjf", "$pat$cur.tar.bz2") and return undef;
    system ("$diff -u $pat$last $pat$cur >upgrade-$last-to-$cur.patch");
    if (WIFSIGNALED(${^CHILD_ERROR_NATIVE}) or
	WEXITSTATUS(${^CHILD_ERROR_NATIVE}) >= 2) { return undef; }
    system ("$gzip -9f --rsyncable upgrade-$last-to-$cur.patch") and return undef;
    system ("rm -rf $pat$last $pat$cur") and return undef;
    system ("gpg -ba --sign upgrade-$last-to-$cur.patch.gz") and return undef;
    unshift @$ary, "    <li><a href=\"upgrade-$last-to-$cur.patch.gz\">$last to\n"
    . "        $cur</a> <a\n"
    . "        href=\"upgrade-$last-to-$cur.patch.gz.asc\">[signature]</a></li>\n";
    return 1;
}
